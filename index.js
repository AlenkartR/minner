const express = require('express');
const app = express();

const default_key = 'EHYWVCnW4XmQxL5dw0O9Noinfd3ZtOAX';

app.set('view engine', 'ejs');
app.set('port', process.env.PORT || 8081);

app.use(express.static('public'));

app.get('/', (req, res) => {
	res.render('index', { key: req.query.key  || default_key});
});

app.listen(app.get('port'), () => {
	console.log(`app is running on port ${app.get('port')}`);
});

